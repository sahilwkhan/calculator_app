import './App.css';
import Header from './components/Header/Header';
import Calculator from './components/calculator';

function App(){

    return (
      <div className="App">
        <Header/>
          <Calculator/>
      </div>
    );
  
}

export default App;
