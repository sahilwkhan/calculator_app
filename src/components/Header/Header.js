import { Component } from "react";
import './Header.css';

class Header extends Component {
    render() {
        return (
            <div className="Header">
                <img  src="https://cdn2.iconfinder.com/data/icons/ios7-inspired-mac-icon-set/512/Calculator_512.png" className="logo"
                />
                <div className="headerName">
                    My_Calculator
                </div>
            </div>
        )
    }
}

export default Header;