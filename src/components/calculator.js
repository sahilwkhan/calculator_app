import './Calculator.css'

import { useState } from "react";

function Calculator() {

    const [updateState, setUpdateState] = useState("");

    const [result, setResult] = useState("");


    // const handleChange = (e) =>{
    //     setUpdateState(updateState.concat( e.target.value));
    // }

    const handleChange = (event) => {

        let buttonClicked = event.target.value;


        if (buttonClicked === 'clear') {
            setUpdateState("");
            setResult("");
        }
        else if (buttonClicked === 'calculate') {
            if (updateState.slice(-1) !== "+" && updateState.slice(-1) !== "-") {
                setResult(eval(updateState));
            }
        }
        else if (buttonClicked === '+' || buttonClicked === "-") {
            if (updateState.length !== 0 && updateState.slice(-1) !== "+" && updateState.slice(-1) !== "-") {
                setUpdateState(updateState.concat(event.target.value));
            }
        }
        else {
            setUpdateState(updateState.concat(event.target.value));
        }
    }




    return (
        <div className="container">
            <div className="frame">
                <div className="digital-display">
                    <div className="upper">
                        {updateState}
                    </div>
                    <div className="lower">
                        {result}
                    </div>
                </div>

                <div className="btn-pad">
                    <div className="controls-pad">
                        <button className="control-btn clear" value='clear' onClick={handleChange}>
                            C
                        </button>
                        <button className="control-btn equal" value='calculate' onClick={handleChange}>
                            =
                        </button>
                    </div>
                    <div className="num-pad">

                        <button className="first digit" value="1" onClick={handleChange}>
                            1
                        </button>
                        <button className="second digit" value="2" onClick={handleChange}>
                            2
                        </button>
                        <button className="third digit" value="3" onClick={handleChange}>
                            3
                        </button>
                        <button className="fourth digit" value="4" onClick={handleChange}>
                            4
                        </button>
                        <button className="fifth digit" value="5" onClick={handleChange}>
                            5
                        </button>
                        <button className="sixth digit" value="6" onClick={handleChange}>
                            6
                        </button>
                        <button className="seventh digit" value="7" onClick={handleChange}>
                            7
                        </button>
                        <button className="eighth digit" value="8" onClick={handleChange}>
                            8
                        </button>
                        <button className="nine digit" value="9" onClick={handleChange}>
                            9
                        </button>
                        <button className="add digit" value="+" onClick={handleChange}>
                            +
                        </button>
                        <button className="ten digit" value="0" onClick={handleChange}>
                            0
                        </button>
                        <button className="substract digit" value="-" onClick={handleChange}>
                            -
                        </button>

                    </div>

                </div>

            </div>
        </div>
    )

}

export default Calculator;